const BASE_URL = "https://api.themoviedb.org/3";
const BACKDROP_BASE_URL = "https://image.tmdb.org/t/p/original/";
const TOTAL_RATING = 5;
const SMALL_IMG_COVER_BASE_URL = "https://image.tmdb.org/t/p/w300/";

export { BASE_URL, BACKDROP_BASE_URL, TOTAL_RATING, SMALL_IMG_COVER_BASE_URL };
